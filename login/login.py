#!/usr/bin/python3
# -*- coding: utf-8 -*-
import services
from PyQt5.QtWidgets import QWidget

from login.login_view import LoginView
from services.crypt import get_hash
from services.messages import show_error
from order.order import Order
from registration.registration import Registration


class Login(QWidget):
    email = ""
    password = ""

    def __init__(self, main_win):
        super().__init__(main_win)
        self.view = LoginView(main_win)
        self.parent = self.parent()
        libs = self.parent.LIBS
        actual_user = libs.get_actual_user()
        if actual_user:
            self.view.email.setText(actual_user['email'])
            self.view.password.setText('123123')
        self.view.sendButton.clicked.connect(self.send_form)

    # отправляем форму
    def send_form(self):
        self.email = self.view.email.text()
        self.password = self.view.password.text()

        if not services.validator.is_valid_email(self.email) or not services.validator.is_valid_password(self.password):
            return

        # если пользователь есть в базе, то переходим на страницу заказа
        # иначе пробуем получить токен с сервера
        # если токен приходит, то делаем запись в БД и переходим на страницу заказа
        # иначе переходим на страницу регистрации
        libs = self.parent.LIBS
        user = libs.get_user_by_email_and_password(self.email, self.password)
        if user:
            self.goto_order(user['token'])
        else:
            self.set_token_from_server()

    # переходим к окну подачи заказа
    def goto_order(self, token):
        self.parent.TOKEN = token
        self.view.central_widget.close()
        Order(self.parent)

    # переходим к окну регистрации
    def goto_registration(self):
        self.view.verticalLayoutWidget.close()
        reg = Registration(self.parent)
        reg.view.email.setText(self.email)
        reg.view.password.setText(self.password)

    # пробуем получить токен с сервера
    def set_token_from_server(self):
        params = [("method", "getToken"), ('email', self.email), ('password', self.password)]
        data = services.http.send_request(params)
        if data['status']:
            user = [{
                'email': self.email,
                'password': get_hash(self.password),
                'token': data['result']['token'],
                'expire_date': data['result']['time'],
            }]
            libs = self.parent.LIBS
            libs.add_or_update_user(user[0])
            self.goto_order(data['result']['token'])
        else:
            show_error('Такого пользователя нет, пожалуйста пройдите регистрацию')
            self.goto_registration()
