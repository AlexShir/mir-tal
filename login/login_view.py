#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5 import QtCore
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton

from services import settings
from services.fonts import get_page_title_font


class LoginView:
    def __init__(self, main_win):

        self.central_widget = QWidget(main_win)

        title = QLabel(self.central_widget)
        title.setText('Авторизация')
        title.setGeometry(QtCore.QRect(0, 10, settings.WIDTH, 20))
        title.setFont(get_page_title_font())
        title.setAlignment(QtCore.Qt.AlignCenter)

        self.verticalLayoutWidget = QWidget(self.central_widget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(200, 100, 500, 200))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)

        email_text = QLabel(self.verticalLayoutWidget)
        email_text.setText("Адрес электронной почты")
        email_text.setAlignment(QtCore.Qt.AlignCenter)
        self.verticalLayout.addWidget(email_text)

        self.email = QLineEdit(self.verticalLayoutWidget)
        self.verticalLayout.addWidget(self.email)

        password_text = QLabel(self.verticalLayoutWidget)
        password_text.setText("Пароль")
        password_text.setAlignment(QtCore.Qt.AlignCenter)
        self.verticalLayout.addWidget(password_text)

        self.password = QLineEdit(self.verticalLayoutWidget)
        self.verticalLayout.addWidget(self.password)

        self.sendButton = QPushButton(self.verticalLayoutWidget)
        self.sendButton.setText("Отправить")
        self.verticalLayout.addWidget(self.sendButton)

        main_win.setCentralWidget(self.central_widget)
        QtCore.QMetaObject.connectSlotsByName(main_win)
