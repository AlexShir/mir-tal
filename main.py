#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys

from PyQt5.QtCore import QCoreApplication, QSize
from PyQt5.QtGui import QIcon, QImage, QPalette, QBrush
from PyQt5.QtWidgets import QMainWindow, QApplication
from login.login import Login
from main_menu.menu import Menu
from services import settings
from services.colors import get_base_text_color
from services.init import init
from services.libs import Libs
from services.settings import IMG_PATH

TOKEN = ''


def get_size():
    return QSize(settings.WIDTH, settings.HEIGHT)


def get_palette():
    palette = QPalette()
    image = QImage(IMG_PATH + 'white-paper-bg.jpg')
    brush = QBrush(image.scaled(get_size()))
    palette.setBrush(QPalette.Window, brush)
    palette.setBrush(QPalette.WindowText, QBrush(get_base_text_color()))

    return palette


class MainWin(QMainWindow):
    def __init__(self):
        super().__init__()
        self.resize(settings.WIDTH, settings.HEIGHT)
        self.setWindowTitle(settings.ORGANIZATION_NAME)
        self.setMinimumSize(get_size())
        self.setMaximumSize(get_size())
        init()
        self.setWindowIcon(QIcon(IMG_PATH + 'favicon.ico'))
        self.setPalette(get_palette())
        self.LIBS = Libs()
        Login(self)
        Menu(self)


if __name__ == "__main__":
    QCoreApplication.setApplicationName(settings.ORGANIZATION_NAME)
    QCoreApplication.setOrganizationDomain(settings.ORGANIZATION_DOMAIN)
    QCoreApplication.setApplicationName(settings.APPLICATION_NAME)

    app = QApplication(sys.argv)
    mw = MainWin()
    mw.show()
    sys.exit(app.exec_())
