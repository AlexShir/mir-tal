from main_menu.menu_view import MenuView


class Menu:
    menu = None

    def __init__(self, main_win):
        super().__init__()
        menu = MenuView(main_win)
        menu.menuExit.triggered.connect(main_win.close)
