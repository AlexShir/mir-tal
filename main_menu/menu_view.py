from PyQt5 import QtWidgets, QtCore
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QMenuBar


def get_font():
    font = QFont()
    font.setFamily('Arial')
    font.setPointSize(10)
    font.setBold(True)

    return font


class MenuView(QMenuBar):
    def __init__(self, main_win):
        super().__init__()
        self.setGeometry(QtCore.QRect(0, 0, 0, 21))
        self.setObjectName("menubar")
        self.setFont(get_font())

        ordersMenu = QtWidgets.QMenu(self)
        ordersMenu.setTitle("Заказы")
        ordersMenu.setObjectName("ordersMenu")

        self.menu_3 = QtWidgets.QMenu(ordersMenu)
        self.menu_3.setTitle("Добавить")
        self.menu_3.setObjectName("menu_3")
        main_win.setMenuBar(self)
        self.statusbar = QtWidgets.QStatusBar(main_win)
        self.statusbar.setObjectName("statusbar")
        main_win.setStatusBar(self.statusbar)
        self.menuKidsOrder = QtWidgets.QAction(main_win)
        self.menuKidsOrder.setText("Заказ для детей")
        self.menuKidsOrder.setIconText("Заказ для детей")
        self.menuKidsOrder.setToolTip("Заказ для детей")
        self.menuKidsOrder.setStatusTip("")
        self.menuKidsOrder.setShortcut("Ctrl+N")
        self.menuKidsOrder.setObjectName("menuKidsOrder")
        self.menuPedOrder = QtWidgets.QAction(main_win)
        self.menuPedOrder.setText("Заказ для педагогов")
        self.menuPedOrder.setIconText("Заказ для педагогов")
        self.menuPedOrder.setToolTip("Заказ для педагогов")
        self.menuPedOrder.setShortcut("Ctrl+M")
        self.menuPedOrder.setObjectName("menuPedOrder")
        self.menuExit = QtWidgets.QAction(main_win)
        self.menuExit.setText("&Выход")
        self.menuExit.setIconText("Выход")
        self.menuExit.setToolTip("Выход")
        self.menuExit.setStatusTip("Выход из приложения")
        self.menuExit.setShortcut("Ctrl+Q")
        self.menuExit.setObjectName("menuExit")
        self.menu_3.addAction(self.menuKidsOrder)
        self.menu_3.addAction(self.menuPedOrder)
        ordersMenu.addAction(self.menu_3.menuAction())
        ordersMenu.addSeparator()
        ordersMenu.addAction(self.menuExit)
        self.addAction(ordersMenu.menuAction())

        QtCore.QMetaObject.connectSlotsByName(main_win)
