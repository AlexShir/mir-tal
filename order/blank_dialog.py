from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QWidget, QVBoxLayout, QHBoxLayout, QGroupBox, QButtonGroup, QPushButton, \
    QScrollArea

from order.info import fill_info
from services.libs import get_blanks
from services.settings import BLANKS_PATH, IMG_PATH
from services.styles import blank_label_style


class BlankDialog(QDialog):
    def __init__(self, ov):
        super().__init__()
        self.ov = ov
        self.setWindowTitle('Выбор бланка диплома')
        self.setWindowIcon(QIcon(IMG_PATH + 'favicon.ico'))
        self.resize(700, 230)
        widget = QWidget(self)
        vert_layout = QVBoxLayout(widget)
        hor_layout = QHBoxLayout()
        group_box = QGroupBox()
        btn_group = QButtonGroup()
        btn_group.setExclusive(True)
        for blank in get_blanks():
            but = QPushButton(widget)
            but.setIcon(QIcon(BLANKS_PATH + blank))
            but.setIconSize(QSize(110, 150))
            but.setStyleSheet(blank_label_style)
            hor_layout.addWidget(but)
            n = int(blank.split('.')[0])
            btn_group.addButton(but, n)
        btn_group.buttonClicked[int].connect(self.selected_blank)

        group_box.setLayout(hor_layout)
        scroll = QScrollArea()
        scroll.setWidget(group_box)
        scroll.setWidgetResizable(True)
        scroll.setFixedHeight(200)
        scroll.setFixedWidth(680)
        vert_layout.addWidget(scroll)
        self.exec_()

    def selected_blank(self, n):
        self.ov.order_blank = n
        fill_info(self.ov)
        self.close()
