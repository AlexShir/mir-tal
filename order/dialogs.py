from PyQt5.QtWidgets import QInputDialog

from order.blank_dialog import BlankDialog
from order.info import fill_info
from order.link_file_dialog import LinkFileDialog
from services.fonts import get_tab_font_combo
from services.messages import show_error


def init_dialogs(ov):
    LinkFileDialog(ov)
    if not show_dialog_kind(ov):
        return
    if not show_dialog_type(ov):
        return
    if not show_dialog_nomination(ov):
        return
    if not show_dialog_theme(ov):
        return
    # детские конкурсы
    if ov.order_type == ov.type_index[ov.type_kid]:
        if not show_dialog_chief_name(ov):
            return
        if not show_dialog_executor_name(ov):
            return
        if not show_dialog_executor_age(ov):
            return
    # конкурсы для педагогов
    else:
        if not show_dialog_executor_name(ov):
            return
    if not show_dialog_work_title(ov):
        return
    if not show_dialog_organization_name(ov):
        return
    if not show_dialog_organization_address(ov):
        return
    BlankDialog(ov)
    # LinkFileDialog(ov)
    return


def prepare_input_dialog(ov, title, label, w, h):
    d = QInputDialog(ov.central_widget)
    d.setWindowTitle(title)
    d.setLabelText(label)
    d.resize(w, h)
    d.setFont(get_tab_font_combo())
    return d


# показываем диалог выбора вида конкурса
def show_dialog_kind(ov):
    d = prepare_input_dialog(ov, 'Выбор вида конкурса', 'Вид конкурса:', 300, 200)
    items = (ov.kind_fast, ov.kind_urgent)
    d.setComboBoxItems(items)
    if ov.order_kind:
        k = ov.kind_fast if ov.kind_index[ov.kind_fast] == ov.order_kind else ov.kind_urgent
        d.setTextValue(k)
    ok = d.exec_()
    kind = d.textValue()
    if ok and kind:
        ov.order_kind = ov.kind_index[kind]
        fill_info(ov)
        return True
    else:
        return False


# показываем диалог выбора типа конкурса
def show_dialog_type(ov):
    d = prepare_input_dialog(ov, 'Выбор типа конкурса', 'Тип конкурса:', 300, 200)
    items = (ov.type_kid, ov.type_ed)
    d.setComboBoxItems(items)
    if ov.order_type:
        k = ov.type_kid if ov.type_index[ov.type_kid] == ov.order_type else ov.type_ed
        d.setTextValue(k)
    ok = d.exec_()
    t = d.textValue()
    if ok and t:
        ov.order_type = ov.type_index[t]
        fill_info(ov)
        return True
    else:
        return False


# показываем диалог выбора номинации
def show_dialog_nomination(ov):
    d = prepare_input_dialog(ov, 'Выбор номинации', 'Номинация:', 300, 200)
    items = []
    for i in ov.nominations[ov.order_type]:
        items.append(i['title'])
    d.setComboBoxItems(items)
    if ov.order_nomination:
        n = ''
        for i in ov.nominations[ov.order_type]:
            if i['id'] == ov.order_nomination:
                n = i['title']
                break
        d.setTextValue(n)
    ok = d.exec_()
    nomination = d.textValue()
    if ok and nomination:
        for i in ov.nominations[ov.order_type]:
            if i['title'] == nomination:
                ov.order_nomination = i['id']
                break
        fill_info(ov)
        return True
    else:
        return False


# показываем диалог выбора темы
def show_dialog_theme(ov):
    d = prepare_input_dialog(ov, 'Выбор темы', 'Тема:', 300, 200)
    items = []
    for i in ov.themes:
        items.append(i['title'])
    d.setComboBoxItems(items)
    if ov.order_theme:
        t = ''
        for i in ov.themes:
            if i['id'] == ov.order_theme:
                t = i['title']
                break
        d.setTextValue(t)
    ok = d.exec_()
    theme = d.textValue()
    if ok and theme:
        for i in ov.themes:
            if i['title'] == theme:
                ov.order_theme = i['id']
                break
        fill_info(ov)
        return True
    else:
        return False


# показываем диалог ввода ФИО руководителя
def show_dialog_chief_name(ov):
    d = prepare_input_dialog(ov, 'Ввод ФИО руководителя', 'ФИО руководителя:', 300, 200)
    d.setInputMode(QInputDialog.TextInput)
    d.setTextValue(ov.order_chief_name)
    ok = d.exec_()
    text = d.textValue()
    if ok:
        if len(text) < 3:
            show_error('ФИО руководителя не может содержать менее 3 символов')
            return show_dialog_chief_name(ov)
        ov.order_chief_name = text
        fill_info(ov)
        return True
    else:
        return False


# показываем диалог ввода ФИО исполнителя
def show_dialog_executor_name(ov):
    d = prepare_input_dialog(ov, 'Ввод ФИО исполнителя', 'ФИО исполнителя:', 300, 200)
    d.setInputMode(QInputDialog.TextInput)
    d.setTextValue(ov.order_executor_name)
    ok = d.exec_()
    text = d.textValue()
    if ok:
        if len(text) < 3:
            show_error('ФИО исполнителя не может содержать менее 3 символов')
            return show_dialog_executor_name(ov)
        ov.order_executor_name = text
        fill_info(ov)
        return True
    else:
        return False


# показываем диалог ввода возраст исполнителя
def show_dialog_executor_age(ov):
    d = prepare_input_dialog(ov, 'Ввод возраста исполнителя', 'Возраст исполнителя:', 300, 200)
    d.setInputMode(QInputDialog.IntInput)
    d.setIntMinimum(1)
    d.setIntMaximum(100)
    d.setIntValue(ov.order_executor_age)
    ok = d.exec_()
    age = d.intValue()
    if ok:
        ov.order_executor_age = age
        fill_info(ov)
        return True
    else:
        return False


# показываем диалог ввода названия работы
def show_dialog_work_title(ov):
    d = prepare_input_dialog(ov, 'Ввод названия работы', 'Название работы:', 300, 200)
    d.setInputMode(QInputDialog.TextInput)
    d.setTextValue(ov.order_work_title)
    ok = d.exec_()
    text = d.textValue()
    if ok:
        if len(text) < 3:
            show_error('Название работы не может содержать менее 3 символов')
            return show_dialog_work_title(ov)
        ov.order_work_title = text
        fill_info(ov)
        return True
    else:
        return False


# показываем диалог ввода названия организации
def show_dialog_organization_name(ov):
    d = prepare_input_dialog(ov, 'Ввод названия организации', 'Название организации:', 300, 200)
    d.setInputMode(QInputDialog.TextInput)
    d.setTextValue(ov.order_organization_name)
    ok = d.exec_()
    text = d.textValue()
    if ok:
        ov.order_organization_name = text
        fill_info(ov)
        return True
    else:
        return False


# показываем диалог ввода адреса организации
def show_dialog_organization_address(ov):
    d = prepare_input_dialog(ov, 'Ввод адреса организации', 'Адрес организации:', 300, 200)
    d.setInputMode(QInputDialog.TextInput)
    d.setTextValue(ov.order_organization_address)
    ok = d.exec_()
    text = d.textValue()
    if ok:
        ov.order_organization_address = text
        fill_info(ov)
        return True
    else:
        return False
