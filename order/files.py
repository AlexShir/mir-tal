import os
from time import time

from PyQt5.QtCore import QFile, QBuffer, Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QFileDialog

from services.messages import show_error
from services.settings import ICONS_PATH, ORDER_FILES_ORIGIN_PATH, ORDER_FILES_THUMB_PATH, MAX_FILES


def get_ext(file_name):
    name = os.path.basename(file_name)
    n = os.path.splitext(name)
    if not len(n) == 2:
        return None
    return n[1].replace('.', '')


# удаляем файл и миниатюру для заказа
def del_order_file(name):
    name = os.path.basename(name)
    if os.path.isfile(ORDER_FILES_THUMB_PATH + name):
        os.remove(ORDER_FILES_THUMB_PATH + name)
    n = os.path.splitext(name)
    files = os.listdir(ORDER_FILES_ORIGIN_PATH)
    for file in files:
        t = os.path.splitext(file)
        if not n[0] == t[0]:
            continue
        os.remove(ORDER_FILES_ORIGIN_PATH + file)


class Files:
    ext_image = 'img'
    ext_text = 'text'
    ext_doc = 'docx'
    ext_pdf = 'pdf'
    ext_xls = 'xlsx'
    ext_ppt = 'pptx'
    ext_audio = 'audio'
    ext_video = 'video'
    ext_archive = 'zip'
    ext = [
        {ext_image: ['png', 'jpg', 'gif', 'tiff']},
        {ext_text: ['txt']},
        {ext_doc: ['doc', 'docx']},
        {ext_pdf: ['pdf']},
        {ext_xls: ['xls', 'xlsx']},
        {ext_ppt: ['ppt', 'pptx']},
        {ext_audio: ['mp3', 'wma']},
        {ext_video: ['mp4', 'mpeg', 'wmv', 'fly', '3gpp']},
        {ext_archive: ['zip', 'gzip']}
    ]

    # копируем файл во временную папку
    def copy_temp_file(self, file_dialog):
        files = os.listdir(ORDER_FILES_THUMB_PATH)
        if len(files) > MAX_FILES:
            show_error('Вы можете загрузить только ' + str(MAX_FILES) + ' файла(ов)')
            return
        ext_str = ''
        for n in self.ext:
            for t in n:
                for i in n[t]:
                    ext_str += ' *.' + i
        file_name = QFileDialog.getOpenFileName(None, 'Выберите файл картинки, видео, аудио или текстовый файл',
                                                'c:\\', filter='(' + ext_str + ')')[0]
        if not file_name:
            return
        ext = get_ext(file_name)
        name = str(int(time()))
        QFile.copy(file_name, ORDER_FILES_ORIGIN_PATH + name + '.' + ext)
        t = self.get_ext_type(file_name)
        if not t:
            return
        # если картинка - создаем миниатюру, иначе добавляем иконку
        if not t == self.ext_image:
            file_name = ICONS_PATH + t + '.png'
            ext = 'png'
        pix_map = QPixmap(file_name).scaled(150, 150, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        buffer = QBuffer()
        pix_map.save(buffer, ext)
        data = buffer.data()
        with open(ORDER_FILES_THUMB_PATH + name + '.' + ext, "wb") as f:
            f.write(data)
        file_dialog.show_img_box()

    # проверяем соответствует ли расширение типу
    def is_ext_type(self, ext, t):
        for i in self.ext[0][t]:
            if i == ext:
                return True
        return False

    # получаем тип расширения по имени файла
    def get_ext_type(self, file_name):
        ext = get_ext(file_name)
        for n in self.ext:
            for t in n:
                for i in n[t]:
                    if i == ext:
                        return t
        return None

    # получаем картинки или соответствующие типу иконки
    def get_images_or_icons(self):
        result = []
        files = os.listdir(ORDER_FILES_THUMB_PATH)
        for file in files:
            t = self.get_ext_type(file)
            if t == self.ext_image:
                result.append(ORDER_FILES_THUMB_PATH + file)
            else:
                result.append(ICONS_PATH + t + '.png')

        return result
