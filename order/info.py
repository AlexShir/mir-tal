from PyQt5.QtGui import QImage

from order.files import Files
from services.settings import BLANKS_PATH


# добавляем строку в информационное поле
def add_info_text(ov, title, value, desc=''):
    cursor = ov.info.textCursor()
    cursor.insertHtml(
        '''<span style="font-size:12px;"><span>{}:</span> 
        <span style="font-weight:bold;">{}</span></span><br>'''.format(title, str(value))
    )
    if not desc == '':
        cursor.insertHtml('''<span>{}</span><br>'''.format(desc))
    cursor.insertHtml('''<br>''')


# заполняем информационное поле
def fill_info(ov):
    ov.info.setText('')
    if ov.order_kind:
        kind = ov.kind_fast if ov.order_kind == ov.kind_index[ov.kind_fast] else ov.kind_urgent
        add_info_text(ov, 'Вид конкурса', kind, ov.descriptions[kind])
    if ov.order_type:
        t = ov.type_kid if ov.order_type == ov.type_index[ov.type_kid] else ov.type_ed
        add_info_text(ov, 'Тип конкурса', t)
        if t == ov.type_ed:
            ov.order_executor_age = 0

    if ov.order_nomination:
        for i in ov.nominations[ov.order_type]:
            if i['id'] == ov.order_nomination:
                add_info_text(ov, 'Номинация', i['title'], i['text'])
                break
    if ov.order_theme:
        for i in ov.themes:
            if i['id'] == ov.order_theme:
                add_info_text(ov, 'Тема', i['title'], i['text'])
                break
    if ov.order_chief_name:
        add_info_text(ov, 'ФИО руководителя', ov.order_chief_name)
    if ov.order_executor_name:
        add_info_text(ov, 'ФИО исполнителя', ov.order_executor_name)
    if ov.order_executor_age:
        add_info_text(ov, 'Возраст исполнителя', ov.order_executor_age)
    if ov.order_work_title:
        add_info_text(ov, 'Название работы', ov.order_work_title)
    if ov.order_organization_name:
        add_info_text(ov, 'Название организации', ov.order_organization_name)
    if ov.order_organization_address:
        add_info_text(ov, 'Адрес организации', ov.order_organization_address)
    if ov.order_blank:
        cursor = ov.info.textCursor()
        cursor.insertHtml('''<span style="font-size:12px;"><span>Бланк диплома:</span>''')
        image = QImage(BLANKS_PATH + str(ov.order_blank) + '.jpg')
        cursor.insertImage(image)
    # ссылки
    if ov.order_link_1:
        add_info_text(ov, 'Ссылка 1', ov.order_link_1)
    if ov.order_link_2:
        add_info_text(ov, 'Ссылка 2', ov.order_link_2)
    if ov.order_link_3:
        add_info_text(ov, 'Ссылка 3', ov.order_link_3)
    # файлы
    icons = Files().get_images_or_icons()
    if len(icons):
        cursor = ov.info.textCursor()
        for icon in icons:
            cursor.insertImage(icon)
