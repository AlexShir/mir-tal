import validators
from PyQt5 import QtCore
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QDialog, QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton, QHBoxLayout

from order import files
from order.files import Files
from order.info import fill_info
from services.messages import show_error
from services.settings import IMG_PATH


class LinkFileDialog(QDialog):
    def __init__(self, ov):
        super().__init__()
        self.ov = ov
        self.setWindowTitle('Ввод ссылки или загрузка файла')
        self.setWindowIcon(QIcon(IMG_PATH + 'favicon.ico'))
        self.resize(500, 500)
        widget = QWidget(self)
        vert_layout = QVBoxLayout(widget)

        text_1 = QLabel(widget)
        text_1.setText('Введите ссылки на файлы, размещенные на стороннем сервере')
        vert_layout.addWidget(text_1)

        self.link_1 = QLineEdit(widget)
        vert_layout.addWidget(self.link_1)
        self.link_2 = QLineEdit(widget)
        vert_layout.addWidget(self.link_2)
        self.link_3 = QLineEdit(widget)
        vert_layout.addWidget(self.link_3)

        link_but = QPushButton(widget)
        link_but.setText('Сохранить ссылки')
        link_but.clicked.connect(self.save_links)
        vert_layout.addWidget(link_but)

        text_2 = QLabel(widget)
        text_2.setText('или загрузите файлы')
        text_2.setContentsMargins(0, 20, 0, 0)
        vert_layout.addWidget(text_2)

        upload_butt = QPushButton(widget)
        upload_butt.setText('Загрузить файл')
        vert_layout.addWidget(upload_butt)
        upload_butt.clicked.connect(lambda: Files().copy_temp_file(self))

        self.text_3 = QLabel(widget)
        self.text_3.setContentsMargins(0, 20, 0, 0)
        vert_layout.addWidget(self.text_3)

        self.img_layout = QHBoxLayout()
        self.img_layout.setGeometry(QtCore.QRect(0, 300, 450, 150))
        vert_layout.addLayout(self.img_layout)

        self.show_img_box()

        self.exec_()

    def save_links(self):
        self.ov.order_link_1 = ''
        self.ov.order_link_2 = ''
        self.ov.order_link_3 = ''
        link_1 = self.link_1.text()
        if link_1:
            if not validators.url(link_1):
                show_error('Неверная ссылка 1')
            else:
                self.ov.order_link_1 = link_1

        link_2 = self.link_2.text()
        if link_2:
            if not validators.url(link_2):
                show_error('Неверная ссылка 2')
            else:
                self.ov.order_link_2 = link_2

        link_3 = self.link_3.text()
        if link_3:
            if not validators.url(link_3):
                show_error('Неверная ссылка 3')
            else:
                self.ov.order_link_3 = link_3

        fill_info(self.ov)

    # отображаем иконки
    def show_img_box(self):
        for i in reversed(range(self.img_layout.count())):
            self.img_layout.itemAt(i).widget().setParent(QWidget())
        icons = Files().get_images_or_icons()
        text = 'Для удаления файла нажмите на иконку' if len(icons) else ''
        self.text_3.setText(text)
        for icon in icons:
            label = QLabel()
            label.setPixmap(QPixmap(icon))
            label.setAccessibleDescription(icon)
            self.img_layout.addWidget(label)
            label.installEventFilter(self)

    # обрабатываем нажатие на иконки
    def eventFilter(self, obj, e):
        if not e.type() == 2:
            return False
        files.del_order_file(obj.accessibleDescription())
        self.show_img_box()
        return True
