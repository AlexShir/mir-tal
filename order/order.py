from PyQt5.QtWidgets import QWidget

from order.dialogs import init_dialogs
from order.order_view import OrderView


class Order(QWidget):

    def __init__(self, main_win):
        super().__init__(main_win)
        self.view = OrderView(main_win)
        view = self.view
        init_dialogs(view)
        view.new_button.clicked.connect(self.new_order)
        view.edit_button.clicked.connect(lambda: init_dialogs(view))

    def new_order(self):
        view = self.view
        view.order_kind = 0
        view.order_type = 0
        view.order_nomination = 0
        view.order_theme = 0
        view.order_chief_name = ''
        view.order_executor_name = ''
        view.order_executor_age = 0
        view.order_work_title = ''
        view.order_organization_name = ''
        view.order_organization_address = ''
        view.order_blank = 0
        view.info.setText('')

        init_dialogs(view)
