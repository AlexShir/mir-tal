# -*- coding: utf-8 -*-

from PyQt5 import QtCore
from PyQt5.QtGui import QPalette, QBrush, QColor
from PyQt5.QtWidgets import QWidget, QLabel, QTextBrowser, QHBoxLayout, QPushButton

from services import settings
from services.colors import get_base_text_color
from services.fonts import get_page_title_font


def get_tab_palette():
    palette = QPalette()
    # цвет текста
    palette.setBrush(QPalette.Active, QPalette.WindowText, QBrush(get_base_text_color()))

    brush = QBrush(QColor(200, 0, 0))
    brush.setStyle(QtCore.Qt.SolidPattern)
    palette.setBrush(QPalette.Inactive, QPalette.Window, brush)

    brush = QBrush(QColor(200, 0, 0))
    brush.setStyle(QtCore.Qt.SolidPattern)
    palette.setBrush(QPalette.Active, QPalette.Window, QBrush(QColor('Dark Blue')))

    return palette


class OrderView:
    def __init__(self, main_win):
        # переменные заказа
        self.order_kind = 0
        self.order_type = 0
        self.order_nomination = 0
        self.order_theme = 0
        self.order_chief_name = ''
        self.order_executor_name = ''
        self.order_executor_age = 0
        self.order_work_title = ''
        self.order_organization_name = ''
        self.order_organization_address = ''
        self.order_blank = 0
        self.order_link_1 = ''
        self.order_link_2 = ''
        self.order_link_3 = ''
        # кнопки
        self.new_button = None
        self.edit_button = None
        self.preview_button = None
        self.send_button = None
        #

        self.kind_fast = 'Быстрый'
        self.kind_urgent = 'Срочный'
        self.kind_index = {
            self.kind_fast: 1,
            self.kind_urgent: 2
        }
        self.descriptions = {
            self.kind_fast: 'Быстрые конкурсы проводятся каждые 2 суток, организационный сбор составляет 100 рублей',
            self.kind_urgent: 'Срочные конкурсы проводятся каждые 12 часов, организационный сбор составляет 150 рублей'
        }
        self.type_kid = 'Конкурсы для детей'
        self.type_ed = 'Конкурсы для педагогов'
        self.type_index = {
            self.type_kid: 1,
            self.type_ed: 2
        }
        self.main_win = main_win
        self.libs = main_win.LIBS
        self.themes = self.libs.get_themes()
        kid_index = self.type_index[self.type_kid]
        ed_index = self.type_index[self.type_ed]
        self.nominations = {
            kid_index: self.libs.get_nominations(kid_index),
            ed_index: self.libs.get_nominations(ed_index)
        }

        self.central_widget = QWidget(main_win)
        # заголовок
        title = QLabel(self.central_widget)
        title.setText('Заявка на конкурс')
        title.setAlignment(QtCore.Qt.AlignCenter)
        title.setGeometry(QtCore.QRect(0, 10, settings.WIDTH, 20))
        title.setFont(get_page_title_font())

        # окно с введенной информацией
        self.info = QTextBrowser(self.central_widget)
        self.info.setGeometry(QtCore.QRect(10, 40, settings.WIDTH - 20, settings.HEIGHT - 350))

        # кнопки
        self.button_widget = QWidget(self.central_widget)
        self.button_widget.setGeometry(QtCore.QRect(10, settings.HEIGHT - 100, settings.WIDTH - 20, 70))
        self.button_box = QHBoxLayout(self.button_widget)
        self.show_button_case_1()

        main_win.setCentralWidget(self.central_widget)

        QtCore.QMetaObject.connectSlotsByName(main_win)

    # набор кнопок, когда заказ оформлен не до конца
    def show_button_case_1(self):
        self.show_new_button()
        self.show_edit_button()

    # набор кнопок, когда заказ оформлен до конца
    def show_button_case_2(self):
        self.show_new_button()
        self.show_edit_button()
        self.show_preview_button()
        self.show_send_button()

    # кнопки
    def show_new_button(self):
        self.new_button = QPushButton(self.button_widget)
        self.new_button.setText('Заполнить заново')
        self.button_box.addWidget(self.new_button)

    def show_edit_button(self):
        self.edit_button = QPushButton(self.central_widget)
        self.edit_button.setText('Редактировать')
        self.button_box.addWidget(self.edit_button)

    def show_preview_button(self):
        self.preview_button = QPushButton(self.central_widget)
        self.preview_button.setText('Предпросмотр диплома')
        self.button_box.addWidget(self.preview_button)

    def show_send_button(self):
        self.send_button = QPushButton(self.central_widget)
        self.preview_button.setText('Отправить заявку')
        self.button_box.addWidget(self.send_button)
