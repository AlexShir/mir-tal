from PyQt5.QtWidgets import QWidget

from services.messages import show_error, show_info
from registration.registration_view import RegistrationView
from services import http
from services import validator


class Registration(QWidget):
    email = ''
    password = ''

    def __init__(self, main_win):
        super().__init__(main_win)
        self.view = RegistrationView(main_win)
        self.view.sendButton.clicked.connect(self.send_form)

    # отправляем форму
    def send_form(self):
        self.email = self.view.email.text()
        self.password = self.view.password.text()

        if not validator.is_valid_email(self.email) or not validator.is_valid_password(self.password):
            return

        params = [('method', 'registration'), ('email', self.email), ('password', self.password)]
        data = http.send_request(params)
        if data:
            if data['status']:
                show_info('Операция успешно завершена, на Ваш электорнный адрес (' + self.email + ') выслано письмо.'
                          + ' Необходимо подтвердить регистрацию, кликнув на ссылку в письме.'
                          + ' После этого выполните вход в систему.')

            else:
                show_error(data['error'])
        else:
            show_error('Ошибка регистрации')
