# -*- coding: utf-8 -*-

from PyQt5 import QtCore
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton, QTabWidget, QStatusBar, QMenuBar


class RegistrationView:
    def __init__(self, main_win):

        self.central_widget = QWidget(main_win)
        self.central_widget.setObjectName("central_widget")

        self.title = QLabel(self.central_widget)
        self.title.setObjectName('title')
        self.title.setGeometry(QtCore.QRect(220, 100, 280, 30))
        self.title.setText('Регистрация')

        self.verticalLayoutWidget = QWidget(self.central_widget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(220, 190, 281, 201))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")

        self.label = QLabel(self.verticalLayoutWidget)
        self.label.setObjectName("label")
        self.label.setText('Email')
        self.verticalLayout.addWidget(self.label)

        self.email = QLineEdit(self.verticalLayoutWidget)
        self.email.setObjectName("lineEdit")
        self.verticalLayout.addWidget(self.email)

        self.label_2 = QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.label_2.setText('Пароль')
        self.verticalLayout.addWidget(self.label_2)

        self.password = QLineEdit(self.verticalLayoutWidget)
        self.password.setObjectName("password")
        self.verticalLayout.addWidget(self.password)

        self.sendButton = QPushButton(self.verticalLayoutWidget)
        self.sendButton.setText("Отправить")
        self.sendButton.setObjectName("pushButton")
        self.verticalLayout.addWidget(self.sendButton)

        main_win.setCentralWidget(self.central_widget)

        QtCore.QMetaObject.connectSlotsByName(main_win)
