from PyQt5.QtGui import QFont


def get_page_title_font():
    font = QFont()
    font.setFamily('Arial')
    font.setPointSize(14)

    return font


def get_tab_font():
    font = QFont()
    font.setFamily('Arial')
    font.setPointSize(10)
    font.setBold(True)
    font.setItalic(True)
    font.setUnderline(True)

    return font


def get_tab_font_combo():
    font = QFont()
    font.setFamily('Arial')
    font.setPointSize(10)

    return font
