from urllib.request import urlopen
from urllib.parse import urlencode

import requests
from services import settings
from services.messages import show_error


def send_request(params):
    if not verify_internet():
        return None
    url = settings.ORGANIZATION_DOMAIN + settings.URL
    headers = {'Content-Type': 'application/json'}
    request = requests.get(url + '?' + urlencode(params), headers=headers)
    if request and request.status_code == 200:
        return request.json()
    else:
        print('Ошибка запроса')

    return None


def verify_internet():
    try:
        urlopen(settings.ORGANIZATION_DOMAIN + settings.URL + '?method=verifyInternet')
        return True
    except IOError:
        show_error('Для работы необходимо подключение к интернету')
        return False


def get_current_timestamp():
    params = [('method', 'getCurrentTimestamp')]
    response = send_request(params)
    if response and response['status']:
        return int(response['result'][0])
    return 0
