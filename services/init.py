import os
import requests
from PyQt5.QtCore import QFile, QIODevice, QTextStream

from services.messages import show_error
from services.settings import ORGANIZATION_DOMAIN, BLANKS_PATH, ORDER_FILES_PATH, LIBS_PATH, IMG_PATH, DATA_PATH, \
    ORDER_FILES_ORIGIN_PATH, ORDER_FILES_THUMB_PATH


def create_dir(path):
    if not os.path.exists(path):
        os.mkdir(path)


def create_file(path, content='{}'):
    if os.path.exists(path):
        return
    file = QFile(path)
    if not file.open(QIODevice.NewOnly):
        show_error('Ошибка создания файла ' + path)
        return
    QTextStream(file) << content


def upload_file(directory, file_name, path):
    if os.path.exists(directory + file_name):
        return
    file = requests.get(ORGANIZATION_DOMAIN + path + file_name)
    f = open(directory + file_name, 'wb')
    f.write(file.content)
    f.close()


def init():
    create_dir(DATA_PATH)
    create_dir(LIBS_PATH)
    create_file(LIBS_PATH + 'nominations.json')
    create_file(LIBS_PATH + 'themes.json')
    create_file(LIBS_PATH + 'users.json')
    create_file(LIBS_PATH + 'update.txt', '0')
    create_dir(IMG_PATH)
    create_dir(BLANKS_PATH)
    create_dir(ORDER_FILES_PATH)
    create_dir(ORDER_FILES_ORIGIN_PATH)
    create_dir(ORDER_FILES_THUMB_PATH)
    upload_file(IMG_PATH, 'favicon.ico', '/')
    upload_file(IMG_PATH, 'white-paper-bg.jpg', '/images/')
