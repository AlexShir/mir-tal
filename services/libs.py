import json
import os
import requests
from PyQt5.QtCore import QFile, QTextStream, QIODevice

from services.crypt import get_hash
from services.http import get_current_timestamp, send_request
from services.settings import ORGANIZATION_DOMAIN, BLANKS_PATH, LIBS_PATH


# получаем бланки
def get_blanks():
    return os.listdir(BLANKS_PATH)


class Libs:
    current_timestamp = 0
    time_diff = 60 * 60 * 24 * 15
    users_file = LIBS_PATH + 'users.json'
    update_file = LIBS_PATH + 'update.txt'
    nominations_file = LIBS_PATH + 'nominations.json'
    themes_file = LIBS_PATH + 'themes.json'

    def __init__(self):
        self.current_timestamp = get_current_timestamp()
        f = QFile(self.update_file)
        f.open(QIODevice.ReadOnly)
        timestamp = QTextStream(f).readAll()
        timestamp = 0 if timestamp == '' else int(timestamp)
        # если время обновления вышло - обновляем
        if timestamp + self.time_diff < self.current_timestamp:
            self.update_libs()

        self.refresh_users()

    # обновляем справочники с сервера если требуется (15 дней)
    def update_libs(self):
        # получаем справочники с сервера и сохраняем в файлах
        params = [('method', 'getLibs')]
        response = send_request(params)
        if not response or not response['status']:
            return
        # обновляем номинации
        f = open(self.nominations_file, 'w')
        f.write(response['result']['nominations'])
        f.close()
        # обновляем темы
        f = open(self.themes_file, 'w')
        f.write(response['result']['themes'])
        f.close()
        # обновляем бланки
        files = os.listdir(BLANKS_PATH)
        for file in files:
            os.remove(BLANKS_PATH + file)
        for b in response['result']['blanks']:
            file = requests.get(ORGANIZATION_DOMAIN + '/images/blanks/' + b['thumb'])
            f = open(BLANKS_PATH + b['id'] + '.jpg', 'wb')
            f.write(file.content)
            f.close()
        # меняем дату обновления
        f = open(self.update_file, 'w')
        f.write(str(self.current_timestamp))
        f.close()

    # получаем номинации
    def get_nominations(self, type_index):
        with open(self.nominations_file, 'r') as file_data:
            data = json.load(file_data)
        t = 'kid' if type_index == 1 else 'educator'
        result = []
        for item in data:
            if item['type'] != t:
                continue
            result.append(item)

        return result

    # получаем темы
    def get_themes(self):
        with open(self.themes_file, 'r') as file_data:
            data = json.load(file_data)

        return data

    # добавляем или меняем пользователя
    def add_or_update_user(self, params):
        with open(self.users_file, 'r') as file_data:
            users = json.load(file_data)
        current_user = params
        index = 0
        if len(users) == 0:
            users = [current_user]
        else:
            for i in range(len(users)):
                if users[i]['email'] == params['email'] and users[i]['password'] == params['password']:
                    index = i
                    break
            if not current_user:
                users = [current_user]
            else:
                current_user['token'] = params['token']
                current_user['expire_date'] = self.current_timestamp + self.time_diff
                users[index] = current_user
        f = open(self.users_file, 'w')
        f.write(json.dumps(users))
        f.close()

    # получаем актуального пользователя (с неистекшим временем жизни токена)
    def get_actual_user(self):
        with open(self.users_file, 'r') as file_data:
            users = json.load(file_data)
        if users == ['']:
            return None
        for user in users:
            if int(user['expire_date']) > self.current_timestamp:
                return user

        return None

    # получаем пользователя по email и паролю
    def get_user_by_email_and_password(self, email, password):
        with open(self.users_file, 'r') as file_data:
            users = json.load(file_data)
        if users == ['']:
            return None
        for user in users:
            if user['email'] == email and user['password'] == get_hash(password) \
                    and int(user['expire_date']) > self.current_timestamp:
                return user

        return None

    # из справочника пользователей удаляем просроченных
    def refresh_users(self):
        with open(self.users_file, 'r') as file_data:
            users = json.load(file_data)
        if len(users) > 0:
            result_users = []
            for user in users:
                if int(user['expire_date']) > self.current_timestamp:
                    result_users.append(user)
            file = QFile(self.users_file)
            file.open(QFile.WriteOnly)
            QTextStream(file) << json.dumps(result_users)
