from PyQt5.QtWidgets import QMessageBox


def show_error(text):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(text)
    msg.setWindowTitle("Ошибка")
    msg.exec_()


def show_info(text):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText(text)
    msg.setWindowTitle("Сообщение")
    msg.exec_()
