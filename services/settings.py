ORGANIZATION_NAME = 'Мир Талантов'
APPLICATION_NAME = 'Mir Talantow desktop'
ORGANIZATION_DOMAIN = 'https://mir-talantow.ru'
URL = '/api/v1'
HEIGHT = 900
WIDTH = 900

IMG_PATH = 'images/'
BLANKS_PATH = IMG_PATH + 'blanks/'
ORDER_FILES_PATH = IMG_PATH + 'order/'
ORDER_FILES_ORIGIN_PATH = ORDER_FILES_PATH + 'origin/'
ORDER_FILES_THUMB_PATH = ORDER_FILES_PATH + 'thumb/'
ICONS_PATH = IMG_PATH + 'icons/'

DATA_PATH = 'data/'
LIBS_PATH = DATA_PATH + 'libs/'

MAX_FILES = 3
