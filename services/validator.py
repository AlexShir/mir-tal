from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QRegExpValidator

from services.messages import show_error


# проверяем введенный email
def is_valid_email(email):
    reg_ex = QRegExp("[\w'._+-]+@[\w'._+-]+")
    validator = QRegExpValidator(reg_ex)
    r = validator.validate(email, 0)
    if r[0] != 2:
        show_error('Неверный формат электронного адреса')
        return False

    return True


# проверяем пароль
def is_valid_password(password):
    password_min_len = 6
    if len(password) < password_min_len:
        show_error('Пароль должен содержать не менее ' + str(password_min_len) + ' символов')
        return False

    return True
